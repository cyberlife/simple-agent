var base = require('cyber-base')

function agentFunc(paramTypes) {

  this.paramTypes = paramTypes

}

var agentDictionary = {

  "sayHello": new agentFunc([]),
  "returnSomeValue": new agentFunc(["number"]),
  "computeStuff": new agentFunc(["object"]),
  "postStuffToIPFS": new agentFunc(["string"])

}

//------------------------------------------------------------------

async function sayHello(jobId, agentName, caller, host, paramsArr) {

  var sanitize = await base.checkAgentFunction(jobId, agentName, caller,
                                               host, agentDictionary["sayHello"].paramTypes,
                                               paramsArr);

  if (!sanitize) return

  console.log("Hello")

}

async function returnSomeValue(jobId, agentName, caller, host, paramsArr) {

  var sanitize = await base.checkAgentFunction(jobId, agentName, caller,
                                               host, agentDictionary["returnSomeValue"].paramTypes,
                                               paramsArr);

  if (!sanitize) return

  await base.dialNode(host, caller, [jobId.toString() + " -- " + paramsArr[0].toString()])

}

async function computeStuff(jobId, agentName, caller, host, paramsArr) {

  var sanitize = await base.checkAgentFunction(jobId, agentName, caller,
                                               host, agentDictionary["computeStuff"].paramTypes,
                                               paramsArr);

  if (!sanitize) return

  var sum = 0

  for (var i = 0; i < paramsArr[0].length; i++) {

    sum += paramsArr[0][i]

  }

  await base.dialNode(host, caller, [jobId.toString() + " -- " + sum.toString()])

}

async function postStuffToIPFS(jobId, agentName, caller, host, paramsArr) {

  var sanitize = await base.checkAgentFunction(jobId, agentName, caller,
                                               host, agentDictionary["postStuffToIPFS"].paramTypes,
                                               paramsArr);

  if (!sanitize) return

  var bufferedParam = Buffer.from(paramsArr[0].toString(), 'utf8')

  await base.postDatatoIPFS(bufferedParam, async function(err, res) {

    await base.dialNode( host, caller, [jobId.toString() + " -- " + res[0].hash.toString('utf8')] )

  })

}

module.exports = {

  sayHello,
  returnSomeValue,
  computeStuff,
  postStuffToIPFS

}
